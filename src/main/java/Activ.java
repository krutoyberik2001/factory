public class Activ implements CellularNetwork {
    @Override
    public double getPriceRate() {
        return 20;
    }

    @Override
    public double getPrice(double minutes) {
        return minutes*getPriceRate();
    }
}
