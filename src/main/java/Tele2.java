public class Tele2 implements CellularNetwork {
    @Override
    public double getPriceRate() {
        return 10;
    }

    @Override
    public double getPrice(double minutes) {
        return minutes*getPriceRate();
    }

}
