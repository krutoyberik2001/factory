public interface CellularNetwork {
    double getPriceRate();
    double getPrice(double minutes);
}
