public class Beeline implements CellularNetwork {
    @Override
    public double getPriceRate() {
        return 15;
    }

    @Override
    public double getPrice(double minutes) {
        return minutes*getPriceRate();
    }
}
