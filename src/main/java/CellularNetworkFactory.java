public class CellularNetworkFactory {
    public static CellularNetwork getNetwork(String type){
        if (type.equals("ACTIV"))
            return new Activ();
        else if (type.equals("TELE2"))
            return new Tele2();
        else if (type.equals("BEELINE"))
            return new Beeline();
        return null;
    }
}
